import {applyMiddleware, combineReducers, createStore} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import weatherReducer from "./reducers/weatherReducer";
import thunk from 'redux-thunk';

const middleware = [
    thunk
]
const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});
let store = createStore(combineReducers({weatherReducer}),
    composeEnhancers(
        applyMiddleware(...middleware)
    )
);
export default store;

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./css/Title.css";

class ForecastTitle extends Component {
  render() {
    return (
        <div>
            <p>{Date(this.props.forecast.location.localtime)}</p>
        </div>
    );
  }
}
const mapStateToProps = (state) => {
    return {
      forecast: state.weatherReducer.forecast,
    };
  };
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
  }
  export default connect(mapStateToProps, mapDispatchToProps)(ForecastTitle);;

import React from 'react';
import './css/App.css'
import Forecast from './Forecast';
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Forecast/>
      </div>
    );
  }
}
export default App;


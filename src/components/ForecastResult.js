import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./css/ForecastResult.css";

class ForecastResult extends Component {
  render() {
    return (
      <div className="resultDiv">
        <div className="imgLabelDiv">
          <img alt="Weather" src={this.props.forecast.current.weather_icons}/>
          <label className="weatherLabel">{this.props.forecast.current.weather_descriptions}</label>
        </div>
        <div>
          <label className="temp">{this.props.forecast.current.temperature}</label>
        </div>
        <div className="other">
          <label className="windSpeed"> Wind Speed: {this.props.forecast.current.wind_speed}</label>
          <label className="humidity">Humidity: {this.props.forecast.current.humidity}</label>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    forecast: state.weatherReducer.forecast,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult);
